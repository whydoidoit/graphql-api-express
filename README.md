#graphql-api-express

This module allows you to create a GraphQL api for an Express application from multiple different modules.  This means your API can be built up piecemeal from different parts of the application and brought together as a single API.

It works by having you declare queries, mutuations and types and then merging together multiple declarations into a single set.

You declare GraphQL api functions and their resolvers at the same time using a simple syntax and then the result is served through express using Apollo Server Express.

The library allows you to declare your API or automatically bootstrap modules containing API declarations as you see fit.

## Installation

`npm install --save https://bitbucket.org/whydoidoit/graphql-api-express`

## Use

You declare queries, mutations and types in response to a call back which is provided a simple API and series of definitions.  Your API surface area should be available as soon as the module is loaded, but may of course use promises or async to resolve.

Each type is mapped into a Javascript object so that multiple different callers can augment the object.  In this way the Query and Mutation types are accessible from multiple apis as are other types.

You declare an API provided by including the library and then using the add method:

```javascript

const gql = require("graphql-api-express");

gql.add(context => {
    /* 
        The context object contains helper functions for most
        common operations as well as typeDefs and resolvers that are objects to 
        contain the types you wish to map 
    */
    
    //Declare a function that multiplies by 10
    context.typeDefs.Query["times10(n: Int!)"] = "Int";
    context.resolvers.Query["times10"] = (obj, {n}) => n * 10;

});

//Using a helper

gql.add(({query}) =>{
    query("times10(n: Int!): Int", (parent, {n}) => n * 10);
});

//Using a function name helper and an object to define multiple

gql.add(({query,api}) =>{
    query({
        [api("times10", ["n: Int!"], "Int")]: (parent, {n}) => n * 10,
        [api("version", "String")]: ()=> "1.0.1" 
    });
});

```

### Installing the server

You add the server to an Express app by passing an app to it:

```javascript
const gql = require("graphql-api-express");
gql.initialize(app);
```

This will start an Apollo Server on the express app at "/graphql".  You can change this 
path by specifying it using the `gql.initialize.path` property.

### Add API Context

##### `typeDefs` defines the types that are available to the api

You use the typeDefs to declare the available types.

```javascript
gql.add(({typeDefs})=>{
    const t = typeDefs.MyType = typeDefs.MyType || {};
    t.version = "String";
    t.number = "Int";
    t["test(n: Int!)"] = "Int"; 
});
```

##### `resolvers` defines the resolvers for each type 

You use the resolves to declare the functions that are executed
to produce the results.

They take the same parameters as an Apollo Server resolver. `(parent, namedArguments, context, info)`. Parent will be null at the top level.

```javascript
gql.add(({resolvers})=>{
    const r = resolvers.MyType = resolvers.MyType || {};
    r.version = () => "1.0.1";
    r.number = () => 10;
    r["test(n: Int!)"] = (parent, {n})=>n * 10; 
});
```

##### `define` provides a shortcut method to define one or more apis on a type

You can use define to quickly declare your API (although there are short cut methods for top level query and mutation).

```javascript
gql.add(({define}) => {
    define("MyType", "version", "String", ()=>"v1.0");
    define("MyOtherType", {
        "version: String": () => "v1.1",
        "test: String": () => "some value" 
    });
});

//Create an easy access curried method to declare an api on a type
gql.add(({define}) => {
    let myType = define("MyType");
    myType({
        "version: String": () => "v1.1",
        "test: String": () => "some value" 
    });
    myType("another(n: Int!)", "String", (parent, {n})=> `value was ${n}`);
});

```

##### `query` provides a shortcut method to define top level queries

Use query to define top level API.

```javascript
gql.add(({query}) => {
    query({
        "version: String": () => "v1.1",
        "test: String": () => "some value" 
    });
});
```

##### `mutation` provides a shortcut method to define top level mutations

See query.


##### `type` provides a shortcut method to define a type using a string

```javascript
gql.add(({query, type}) => {
    type("MyType", `
       value: Int
       data: String
    `)
    query({
        "test: MyType": () => ({value: 1, data: "Some data"}) 
    });
});
```

##### `api` is a helper method to declare a method string

You can use the `api` function to declare method names

Usage: 
* api(name, type)
* api(name, parametersAsStringOrArray, type)

```javascript
gql.add(({query}) => {
    query({
        [api("version", "String")]: () => "1.0.1",
        [api("fn", "n1: Int!, n2: Int!", "Int")]: (parent, {n1, n2})=>n1 * n2,
        [api("fn2", ["n1: Int!", "n2: Int!"])]: (parent, {n1, n2})=>n1 + n2
    });
});
```

