const gqlApi = require("./gql-api");

module.exports = {
    initialize: gqlApi,
    ready() { return gqlApi.promise },
    add(fn) {
        gqlApi.events.on("ready", fn);
    },
    reset: gqlApi.reset
};


