const gqlApi = require("../../index.js");
gqlApi.add(
    ({query, api}) => {
        query({
            [api("version", "String")]: (obj, {n}) => "1.0.9"
        });
    }
);
