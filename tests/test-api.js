const http = require("http");
const {expect} = require("chai");
const {request} = require("graphql-request");

function requireUncached(module) {
    delete require.cache[require.resolve(module)];
    return require(module);
}

describe("GQL API", function () {
    let server = null;
    let gqlApi = null;

    beforeEach(function (done) {
        const app = requireUncached("./app");
        gqlApi = requireUncached("../index.js");
        gqlApi.initialize(app);
        server = http.createServer(app);
        server.once("listening", done);
        server.listen(3010);
    });
    afterEach(function (done) {
        server.once("close", done);
        server.close();
        gqlApi.reset();
        gqlApi = null;
    });

    it("should return a version string given a query definition", async () => {
        gqlApi.add(
            ({query, api}) => {
                query({
                    [api("version", "String")]: () => "1.0.1"
                });
            }
        );
        await gqlApi.ready();
        const query = `{
            version
        }`;
        let data = await request("http://localhost:3010/graphql", query);
        expect(data.version).to.equal("1.0.1");

    });

    it("should return a version string given a stated call", async () => {
        gqlApi.add(
            ({query}) => {
                query("version", "String", () => "1.0.2");
            }
        );
        await gqlApi.ready();
        const query = `{
            version
        }`;
        let data = await request("http://localhost:3010/graphql", query);
        expect(data.version).to.equal("1.0.2");
    });

    it("should support a query that has parameters", async () => {
        gqlApi.add(
            ({query}) => {
                query("version(n: Int!)", "String", (obj, {n}) => "1.0." + n);
            }
        );
        await gqlApi.ready();
        const query = `{
            version(n: 3)
        }`;
        let data = await request("http://localhost:3010/graphql", query);
        expect(data.version).to.equal("1.0.3");
    });

    it("should be able to declare and return a type", async () => {
        gqlApi.add(
            ({query, type}) => {
                query("version", "VersionInfo", () => ({info: "1.0.4", data: "DATA"}));
                type("VersionInfo", ` 
                    info: String
                    data: String
                 `);
            }
        );
        await gqlApi.ready();
        const query = `{
            version { 
                info 
            }
        }`;
        const query2 = `{
            version { 
                info
                data 
            }
        }`;
        let data = await request("http://localhost:3010/graphql", query);
        expect(data.version.info).to.equal("1.0.4");
        expect(data.version.data).to.not.exist;
        data = await request("http://localhost:3010/graphql", query2);
        expect(data.version.info).to.equal("1.0.4");
        expect(data.version.data).to.equal("DATA");
    });

    it("should be able to define a method on a sub type", async () => {
        gqlApi.add(
            ({query, type, define, api}) => {
                query("version", "VersionAccess", () => ({"test": 1}));
                define("VersionAccess", {
                    [api("banana", "n: Int!", "VersionInfo")]: (obj, {n}) => {
                        expect(obj.test).to.equal(1);
                        return {info: "1.0." + n, data: "DATA"};
                    }
                });
                type("VersionInfo", ` 
                    info: String
                    data: String
                 `);
            }
        );
        await gqlApi.ready();
        const query = `{
            version { 
                banana(n: 6) {
                    info
                }
            }
        }`;
        let data = await request("http://localhost:3010/graphql", query);
        expect(data.version.banana.info).to.equal("1.0.6");

    });

    it("should take an array of parameters for a api helper", async () => {
        gqlApi.add(
            ({query, api}) => {
                query({
                    [api("version", ["n: Int!"], "String")]: (obj, {n}) => "1.0." + n
                });
            }
        );
        await gqlApi.ready();
        const query = `{
            version(n: 7)
        }`;
        let data = await request("http://localhost:3010/graphql", query);
        expect(data.version).to.equal("1.0.7");
    });

    it("should bootstrap modules if given a path", async () => {
        gqlApi.initialize.addScanPath(__dirname);
        await gqlApi.ready();
        const query = `{
            version
        }`;
        let data = await request("http://localhost:3010/graphql", query);
        expect(data.version).to.equal("1.0.8");
    });

    it("should support a different file pattern for bootstrap", async () => {
        gqlApi.initialize.addScanPath(__dirname);
        expect(gqlApi.initialize.pattern).to.equal("**/gqlapi-*");
        gqlApi.initialize.pattern = "**/apitest-*";
        await gqlApi.ready();
        const query = `{
            version
        }`;
        let data = await request("http://localhost:3010/graphql", query);
        expect(data.version).to.equal("1.0.9");
    });

    it("should curry define for a type", async () => {
        gqlApi.add(
            ({query, type, define, api}) => {
                query("version", "VersionAccess", () => ({"test": 1}));
                let versionAccess = define("VersionAccess");
                versionAccess({
                    [api("banana", "n: Int!", "VersionInfo")]: (obj, {n}) => {
                        expect(obj.test).to.equal(1);
                        return {info: "1.0." + n, data: "DATA"};
                    }
                });
                type("VersionInfo", ` 
                    info: String
                    data: String
                 `);
            }
        );
        await gqlApi.ready();
        const query = `{
            version { 
                banana(n: 10) {
                    info
                }
            }
        }`;
        let data = await request("http://localhost:3010/graphql", query);
        expect(data.version.banana.info).to.equal("1.0.10");
    });

    it("should allow a different mount path", async () => {
        expect(gqlApi.initialize.path).to.equal("/graphql");
        gqlApi.initialize.path = "/testql";
        gqlApi.add(
            ({query, api}) => {
                query({
                    [api("version", "String")]: () => "1.0.11"
                });
            }
        );
        await gqlApi.ready();
        const query = `{
            version
        }`;
        let data = await request("http://localhost:3010/testql", query);
        expect(data.version).to.equal("1.0.11");


    });

});
