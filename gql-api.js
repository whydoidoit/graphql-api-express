const {ApolloServer, gql} = require("apollo-server-express");
const {EventEmitter2} = require("eventemitter2");
const walkSync = require("walk-sync");
const map = require("lodash/map");
const isFunction = require("lodash/isFunction");
const isObject = require("lodash/isObject");
const isString = require("lodash/isString");

let events = new EventEmitter2({wildcard: true, maxListeners: 1000});


function processParams(params) {
    if (Array.isArray(params)) {
        return params.join(", ");
    } else {
        return params;
    }
}

const paths = [];
let path = "/graphql";
let pattern = "**/gqlapi-*";
let resolver;
let promise = new Promise((newResolver) => {
    resolver = newResolver;
});

module.exports = function (app) {
    setTimeout(async function () {
        paths.forEach(path => {
            try {
                let apiSources = walkSync(path, {globs: [pattern]}).map(path => path.split(".js")[0]);
                apiSources.forEach(src => require(`${path}/${src}`));
            } catch (e) {
                console.warn(`${path} scan yielded error ${e.message}`);
            }
        });
        try {
            let data = {
                typeDefs: {Query: {}}, resolvers: {
                    Query: {}
                },
                api(name, params, type) {
                    return type ? (`${name}${params ? "(" + processParams(params) + ")" : ""}: ${type}`) : `${name}: ${params}`;
                },
                query(name, type, resolver) {
                    return data.define("Query", name, type, resolver);
                },
                mutation(name, type, resolver) {
                    return data.define("Mutation", name, type, resolver);
                },
                define(Type, name, type, resolver) {
                    if (!name && !type) {
                        return (name, type, resolver) => {
                            data.define(Type, name, type, resolver);
                        };
                    }
                    data.typeDefs[Type] = data.typeDefs[Type] || {};
                    data.resolvers[Type] = data.resolvers[Type] || {};
                    if (isObject(name)) {
                        Object.keys(name).forEach(key => {
                            data.define(Type, key, name[key]);
                        });
                    } else if (!isFunction(type)) {
                        data.typeDefs[Type][name] = type;
                        data.resolvers[Type][name.split("(")[0].trim()] = resolver;
                    } else {
                        let lastColon = name.lastIndexOf(":");
                        data.typeDefs[Type][name.slice(0, lastColon)] = name.slice(lastColon + 1);
                        data.resolvers[Type][name.split("(")[0].split(":")[0].trim()] = type;
                    }
                },
                type(name, declaration) {
                    return data.typeDefs[name] = isString(declaration) ? declaration : Object.assign(data.typeDefs[name] ||  {}, declaration);
                }
            };

            await events.emitAsync("ready", data);
            let defs = map(data.typeDefs, (def, name) => {
                let output = "";
                if (isObject(def)) {
                    output = `
type ${name} {
${map(def, (output, field) => {
                        return `    ${field}: ${output}`;
                    }).join("\n")}
}
`;
                } else if (isString(def)) {
                    output = `type ${name} {
   ${def}
}`;
                }
                return output;
            }).join("\n");

            const typeDefs = gql(defs);
            const resolvers = data.resolvers;
            let server = new ApolloServer({typeDefs, resolvers});
            server.applyMiddleware({app, path});
            resolver(server);
        } catch (e) {
            console.error(e);
        }
    });
};

module.exports.reset = () => {
    promise = new Promise((newResolver) => {
        resolver = newResolver;
    });
    events = new EventEmitter2({wildcard: true, maxListeners: 1000});
};

module.exports.addScanPath = function (path) {
    paths.push(path);
};
Object.defineProperty(module.exports, "pattern", {
    get() {
        return pattern;
    },
    set(v) {
        pattern = v;
    }
});
Object.defineProperty(module.exports, "events", {
    get() {
        return events;
    }
});
Object.defineProperty(module.exports, "promise", {
    get() {
        return promise;
    }
});
Object.defineProperty(module.exports, "path", {
    get() {
        return path;
    },
    set(v) {
        path = v;
    }
});
